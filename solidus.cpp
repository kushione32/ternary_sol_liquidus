//
//  solidus.cpp
//  condensing phase plot
//  Feb 11, 2015 by Yoshinori Miyazaki
//

#include "./melt_chi2.h"
#include "./melt_optSD.h"

int main() {

    /* 
       INPUT TEMPERATURE and PRESSURE from the console
     */
    double T, P, Mgn, Fen;
    cout << "input tempeature [K]: ";
    cin >> T;
    
    cout << "input pressure [Pa]: ";
    cin >> P;
    
    cout << "input Mg# = (Mg[0-1]: ";
    cin >> Mgn;

    cout << "input Fe# = [0-1]: ";
    cin >> Fen;

    /* 
       SET COMPOSITION HERE.
       This program calculates the liquid density for the MgO-SiO2-FeO ternary system.
       
       tensor `q` represents the MOLAR ratio between MgO (q[0]), SiO2 (q[1]), and FeO (q[2]).
       
       (this program does not deal with Fe3+. only Fe2+.)
    */
    tensor1d<double> q(0.0, 3);
    q[0] = Mgn;  q[1] = Sipercent;  q[2] = Fen;
    
    /* --------------------------------------------------------
       OpenMP */
    int NUM_THREADS = omp_get_num_procs();
    omp_set_num_threads(NUM_THREADS);
    
    /* --------------------------------------------------------
       initial parameter set
        (see Miyazaki and Korenaga 2019a for details)
       ... W0[0, 1]     <- A_(Mg0), A_(FeO)
       .   W0[2--7]     <- parameters for SiO2 (K0, K0', a, C0, C1, d)
       .   W0[8, 9, 10] <- Margules parameter for MgO-SiO2 mixing (W0, V0, S0)
       .   W0[11,12,13] <- Margules parameter for FeO-SiO2 mixing (W0, V0, S0)
    */
    chi2SD_model     model;
    tensor1d<double> W0i(0.0,14);
    W0i[0] = 0.963606;      W0i[1] = -0.28607;
    W0i[2] = 2.62623e+10;   W0i[3] = 4.25195;       W0i[4] = 1.54054e-05;
    W0i[5] = 3.32459;       W0i[6] = 0.0559212;     W0i[7] = 0.238776;
    W0i[8] = -126705;       W0i[9] = 1.31702e-06;   W0i[10] = 3.44128;
    W0i[11] = -99917.3;     W0i[12]= 4.71325e-07;   W0i[13] = 52.9655;
    model.setW0(W0i);
    
    // separate data
    tensor1d<double> dG(0.0,8), Wm(0.0,6);
    for (int i=0; i<(int)dG.size(); i++){ dG[i] = W0i[i];   }
    for (int i=0; i<(int)Wm.size(); i++){ Wm[i] = W0i[i+8]; }
        
    /* calculate density 

     ... from the law of thermodynamics V = dG/dP */
    double eP = 1e5;
    tensor1d<double> qinit = q;
    MoleculeData_G list("./list_liquid.txt", T, P, dG);
    gibbsminCG     res(list, q, Wm);
    double Gm = res.getGbest()*(R*T);
    
    q = qinit;
    MoleculeData_G list_pert("./list_liquid.txt", T, P+eP, dG);
    gibbsminCG     res_pert(list_pert, qinit, Wm);
    double Gmd = res_pert.getGbest()*(R*T);
    
    // volume
    double V = (Gmd-Gm)/eP;
    double molar = 40.304e-3*qinit[0] + 60.08e-3*qinit[1] + 71.844e-3*qinit[2];
    
    cout << molar/V << endl;
    
}
