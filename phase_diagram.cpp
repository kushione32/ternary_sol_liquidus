/*
  solidus.cpp
  condensing phase plot
  Feb 11, 2018  by Yoshi Miyazaki
*/

#include "./melt_chi2.h"
#include "./melt_optSD.h"

int main() {
    /* 
       This program calculates the phase-diagram for the (Mg,Fe)O-SiO2 pseudo-binary system.
       The output file (which can be set below) shows
       1. mole % of MgO
       2. mole % of SiO2
       3. mole % of FeO
       4. solidus
       5. liquidus
    */
    
    // INPUT PRESSURE and MG-NUMBER from the console.
    double P, Mgn;
    cout << "input pressure [Pa]: ";
    cin >> P;
    
    cout << "input Mg# = MgO/(MgO+FeO) [0-1]: ";
    cin >> Mgn;
    
    double Pin = P*1e9; // convert to GPa -> Pa
    
    // OUTPUT 
    string filename = "./diagram.txt";
    ofstream fout(filename, ios::trunc);
    
    /* 
       SET COMPOSITION HERE.
       
       tensor `q` represents the MOLAR ratio between MgO (q[0]), SiO2 (q[1]), and FeO (q[2]).
       
       (this program does not deal with Fe3+. only Fe2+.)
    */
    
    /* --------------------------------------------------------
       OpenMP */
    int NUM_THREADS = omp_get_num_procs();
    omp_set_num_threads(NUM_THREADS);
        
    /* --------------------------------------------------------
       initial parameter set
       (see Miyazaki and Korenaga 2019a for details.)
       (the parameters written below is the case of "n=1, w/o chondritic PM" shown in Table 2)
       ... W0[0, 1]     <- A_(Mg0), A_(FeO)
       .   W0[2--7]     <- parameters for SiO2 (K0, K0', a, C0, C1, d)
       .   W0[8, 9, 10] <- Margules parameter for MgO-SiO2 mixing (W0, V0, S0)
       .   W0[11,12,13] <- Margules parameter for FeO-SiO2 mixing (W0, V0, S0)
    */
    chi2SD_model     model;
    tensor1d<double> W0i(0.0,14);
    W0i[0] = 0.963606;      W0i[1] = -0.28607;
    W0i[2] = 2.62623e+10;   W0i[3] = 4.25195;       W0i[4] = 1.54054e-05;
    W0i[5] = 3.32459;       W0i[6] = 0.0559212;     W0i[7] = 0.238776;
    W0i[8] = -126705;       W0i[9] = 1.31702e-06;   W0i[10] = 3.44128;
    W0i[11] = -99917.3;     W0i[12]= 4.71325e-07;   W0i[13] = 52.9655;
    model.setW0(W0i);

    
    // separate data
    tensor1d<double> dG(0.0,8), Wm(0.0,6);
    for (int i=0; i<(int)dG.size(); i++){ dG[i] = W0i[i];   }
    for (int i=0; i<(int)Wm.size(); i++){ Wm[i] = W0i[i+8]; }
    
    /* 
       calculate sollidus/liquidus for different 
     */
    double M = 21;
    cout << "pressure = " << Pin/1e9 << " , " << filename << endl;
    for (int j=0; j<(int)M; j++){
        double           rMg = 0.55 + 0.40*(double)j/(M-1);
        tensor1d<double> qinit(0.0,3);
        qinit[0] = rMg*Mgn;  qinit[1] = 1-rMg; qinit[2] = rMg*(1-Mgn);
        
        /* liquidus and solidus */
        double Tsol = model.mu_meltT("peridotite_s", Pin, qinit);
        double Tliq = model.mu_meltT("peridotite_l", Pin, qinit);
        double TMgO = model.mu_meltT("MgFeO_l",      Pin, qinit);
        // double TMgs = model.mu_meltT("MgFeO_s",      Pin, qinit);
        double TSiO = model.mu_meltT("SiO2_t",       Pin, qinit);
        
        // cout << rMg << "  " << Tsol << "  " << Tliq << "  " << TMgO << "  " << TSiO << "  " << TMgs << endl;
        fout << qinit[0] << "\t" << qinit[1] << "\t" << qinit[2] << "\t" << Tsol << "  " << maxv(Tliq, TMgO) << endl;
    }
}
