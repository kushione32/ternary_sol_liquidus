/*
  SD_chi2.cpp
  ... inheritance of `melt_chi2` class.
  --- add functions to obtain a best-fit set of parameters
  
  Jul 5, 2018
 */

#include "melt_optSD.h"

chi2SD_model::chi2SD_model(){ par = W0; }
chi2SD_model& chi2SD_model::operator=(const chi2SD_model& copy){
    if (this != &copy){
        par      = copy.par;
        scale    = copy.scale;
    }
    return *this;
}

/*------------------------------------------------------------
  preparation for optimization
  ------------------------------------------------------------*/
void chi2SD_model::set_default_scale(){
    /* set scaling factor "scale" */
    scale.resize(14);
    scale[0] = 1e0;   scale[1] = 1e0;   scale[2] = 2e10;  scale[3] = 1e0;
    scale[4] = 2e-5;  scale[5] = 1e0;   scale[6] = 1e-2;  scale[7] = 1e1;
    scale[8] = 2e5;   scale[9] = 1e-6;  scale[10]= 1e1;
    scale[11]= 2e6;   scale[12]= 1e-5;  scale[13]= 1e2;
}
void chi2SD_model::set_rootCp_scale(){
    /* set scaling factor "scale" */
    scale.resize(14);
    scale[0] = 1e-1;  scale[1] = 1e0;   scale[2] = 2e10;  scale[3] = 1e0;
    scale[4] = 2e-5;  scale[5] = 1e1;   scale[6] = 1e1;   scale[7] = 1e1;
    scale[8] = 2e5;   scale[9] = 1e-6;  scale[10]= 1e1;
    scale[11]= 2e6;   scale[12]= 1e-5;  scale[13]= 1e2;
}
void chi2SD_model::show_par(tensor1d<double>& norm_par, double chi){
    /* output to console/file the parameter in non-normalized format */
    ofstream fout("sd_chi2_elapse.txt", ios::app);
    
    for (int i=0; i<(int)par.size(); i++){
        cout << norm_par[i]*scale[i] << "  ";  fout << norm_par[i]*scale[i] << "  ";
    }
    cout << chi << endl;
    fout << chi << endl;
    fout.close();    
}
tensor1d<double> chi2SD_model::optimize_SD(){
    /* scale the parameters with scale */
    par = W0;
    tensor1d<double> norm_par = W0/scale, grad_chi2, grad_old;
    double chi0;
    tie(grad_chi2, chi0) = neg_grad(norm_par);
    grad_old = grad_chi2;
    cout << "init: " << chi0 << endl;
    
    /* the steepest descent method.
       ... repeat the iteration for 200 times unless 
       --- ||grad chi2|| becomes smaller than `eps`   */
    int  count = 0;
    double eps = 1e-3, dd = grad_chi2.norm();
    while (dd > eps && count < 200){
        cout << endl << "step." << count << endl;
        count++;
        
        double            chi1 = 0;
        tensor1d<double>  norm_opp(0.0,14);
        opposite_end(chi0, norm_par, chi1, norm_opp, grad_chi2);
        
        /* find the min in (norm_par, norm_opp) using golden section search */
        tensor1d<double>  dn = norm_opp - norm_par;
        double lim = 1e-3;
        cout << "  gss section. dn .norm = " << dn.norm() << endl;
        cout << "  v0: " << flush;  show_par(norm_par, chi0);	
        cout << "  v1: " << flush;  show_par(norm_opp, chi1);
        if (chi1 == chi0 && dn.norm() < lim){
            /* if chi1 == chi0 & dn.norm() < lim,
               norm_par and n_opp is too close and not worth conducting gss.
               1) check the possibility of this being a local minimum */
            tensor1d<double>  dmin(0.0, (int)par.size());
            bool ismin = is_minimum(norm_par, dmin);
            
            /* 2) if not, perturb the paramter with eps in the direction
               we know that it's going to decrease. */
            if (ismin){ break; }
            else { grad_chi2 = dmin;  continue; }
            
        }else{ /* golden section search */
            tensor1d<double>  n_min = gss(chi0, norm_par, chi1, norm_opp);
            norm_par = n_min;
            
            /* next direction */
            tie(grad_chi2, chi0) = neg_grad(norm_par);   /* <- calc and store chi2 */
            double coeff = grad_chi2*(grad_chi2-grad_old)/(grad_old*grad_old);
            grad_chi2 += grad_old*coeff;
            grad_old   = grad_chi2 - grad_old*coeff;
        }
        
        dd = grad_chi2.norm();
        show_par(norm_par, chi0);
    }
    cout << " gss done. dd = " << dd << " , count = " << count << endl;
    printf("%f  %f  , norm = %f \n", chi0, chi0, grad_chi2.norm());
    
    tensor1d<double>  par_out(0.0,(int)par.size());
    for (int i=0; i<(int)par.size(); i++){ par_out[i] = norm_par[i]*scale[i]; }
    
    return par_out;
}
tensor1d<double> chi2SD_model::grad(int id1, int id2, const tensor1d<double>& n_par){
    int dim = (int)n_par.size();
    if (id2 >= dim){
        cout << "chi2CG.cpp/grad(id): id too large." << endl; std::exit(2);
    }

    tensor1d<double> grad_chi2(0.0,dim), n_pert1 = n_par, n_pert2 = n_par;
    
    double eps = 1e-3;
    double chi2_org = calc_chi2(n_par);
    
    n_pert1[id1] += eps;                     n_pert2[id2] += eps;
    double chi2_1 = calc_chi2(n_pert1),      chi2_2 = calc_chi2(n_pert2);
    grad_chi2[id1] = (chi2_1 - chi2_org);    grad_chi2[id2] = (chi2_2 - chi2_org);
    
    double norm = grad_chi2.norm();
    return grad_chi2/(norm*1e2);
}
tuple<tensor1d<double>, double> chi2SD_model::neg_grad(const tensor1d<double>& n_par){
    int    dim      = (int)n_par.size();
    double chi2_org = calc_chi2(n_par);
    
    /* set chi2 in the class here! */
    chi2 = chi2_org;
    cout << "org: " << chi2_org << endl;
    
    /* perturb each parameter and calculate the gradient numerically */
    double eps = 1e-3;
    tensor1d<double>  grad_chi2(0.0,n_par.size());
    for (int i=0; i<dim; i++){
        tensor1d<double>  n_pert = n_par;
        n_pert[i] += eps;
        
        double chi2_pert = calc_chi2(n_pert);
        double dchi2 = chi2_pert - chi2_org;
        grad_chi2[i] = dchi2/eps;
        
        /* check for any potential numerical error */
        cout << "*" << flush;
        if (abs(dchi2) > 2e3){
            cout << "error. investigate. " << chi2_pert << endl;
            chi2_pert = calc_chi2(n_pert);
            dchi2 = chi2_pert - chi2_org;
            if (abs(dchi2) < 100){ 
                grad_chi2[i] = dchi2/eps;
                continue;
            }
            //exit(2);
        }
    }
    
    cout << " --- grad: " << endl;
    for (int i=0; i<dim; i++){
        cout << i << " - " << grad_chi2[i] << endl;
    }
    
    /* shrink the gradient vector & *(-1) for descending direction.
       norm of 1 is too large to search for the next step... */
    double norm = grad_chi2.norm();
    grad_chi2 /= (-1.0e3);
    
    cout << "   norm = " << grad_chi2.norm() << endl;
    
    return forward_as_tuple(grad_chi2, chi2);
}
double chi2SD_model::calc_chi2(const tensor1d<double>& par){
    /* set private-member W0 and calculate the chi2 */
    int dim = (int)par.size();
    for (int i=0; i<dim; i++){ W0[i] = par[i]*scale[i]; }
    
    chi2 = calc_chi2();
    return chi2;
}
void chi2SD_model::opposite_end(double& chi2_in,  tensor1d<double>& v_in,
                                double& chi2_out, tensor1d<double>& v_out,
                                const tensor1d<double>& direc){
    /* check whether the range (v_in, v_in+direc) includes the minimum
       1. shrink v1=v0+dv to meet the a-priori range. */
    tensor1d<double> v0 = v_in, dv = direc,    v1 = v_in + direc;
    double          mod = avoidneg(v0,dv,1);   v1 = v_in + dv;
    cout << "  gradient, mod= " << mod << endl;
    for (int i=0; i<v1.size(); i++){ cout << dv[i]*scale[i] << "  " << flush; }
    cout << endl;
    
    /* 2. compare the chi2 at (v0) and (v1 = v0+dv).
       ... here, chi0 = chi2 of v0,  chi1 = chi2 of v1 */
    double chi0 = chi2_in,  chi2_prev = chi2_in;
    double chi1 = calc_chi2(v1);
    cout << "  <opposite_end>: initial two end: " << endl;
    show_par(v0, chi0);
    show_par(v1, chi1);
    
    /* 3. if chi1 is smaller than chi0,
       --- it is likely that the minimum can be outside the range of (v0, v1).
       
       Now compare chi2 of (v0+dv,v0+2dv), (v0+2dv, v0+3dv), and so on...
       If chi1 > chi0, the minimum should be somewhere in (v0-dv, v0+dv)       */
    double eps = 1e-12;
    while (chi1 < chi0 && (1.0-mod) < eps){
        v0 = v1;       chi2_prev = chi0;
        v1 = v0+ dv;   chi0      = chi1;
        
        /* assure v0+dv will not violate non-neg condition 
           ... and update v1 w/ shrinked dv. */
        mod  = avoidneg(v0,dv,0);
        v1   = v0 + dv;
        chi1 = calc_chi2(v1);
        
        cout << " extending: ";  show_par(v1, chi1);
    }
    
    /* When v0 == v_in, chi1 should be larger than chi2_0.
       However, if it's too large, gss may be a waste of calculation time.
       here, we keep dividing the section into half until chi1 ~ chi0
       
       returning (v_in, v_out) will be (v0,v1) or (v0,v1/2^n) */
    if (v0 == v_in){
        cout << "  <oppsoite_end> divide the section. chi0= ";
        cout << chi0 << " , chi1= " << chi1 << endl;
        
        double chi2_save = chi1;
        while (chi1 > chi0){
            dv  /= 2.0;
            v1   = v0 + dv;
            chi1 = calc_chi2(v1);
            show_par(v1, chi1);
            
            if (chi1 < chi0){
                v1 = v0 + dv*2;      /* dv is too small. restore the prev step size. */
                chi1 = chi2_save;  /* and restore the previous chi2 */
                break;
            } else if(chi1 == chi0){
                break;
            }else {
                chi2_save = chi1;
            }
        }
    }
    /* When v0 != v_in indciates that min is in (v0-dv,v1).
       In this scenario, chi2(v0) < chi2(v1) should be holding.
       The minimum should be in (v0-dv, v1), so restore the previous value. */
    else{
        v0 -= dv; chi0 = chi2_prev;
    }
    
    cout << "opp, chi1= " << chi1 << " , chi0=" << chi0 << endl;
    
    v_in  = v0;  chi2_in  = chi0;
    v_out = v1;  chi2_out = chi1;
    
    return;
}
double chi2SD_model::avoidneg(const tensor1d<double>& v0, tensor1d<double>& dv, bool isfirst){
    /* adjust parameter so that it is in a physically plausible range */
    tensor1d<double> v1 = v0 + dv;
    
    double tmp, mod = 1.0;
    for (int i=0; i<v0.size(); i++){
        tmp = 1.0;
        double org0 = v0[i]*scale[i],  org1 = v1[i]*scale[i];
        
        /* pressure dependence of entropy change upon melting: d(DS)/dP = (-1,1) */
        if (i==0 || i==1){
            if (org1 < -1){ tmp = (org0+1)/(org0+org1); }
            if (org1 >  1){ tmp = (1-org0)/(org1-org0); }
        }
        /* bulk modulus: K0 = 15GPa - 200GPa */
        else if (i==2){
            if (org1 < 15e9){ tmp =  (org0-15e9)/(org0-org1);  cout << "K0, " << tmp << " "; }
            if (org1 >200e9){ tmp = (200e9-org0)/(org1-org0); }
        }
        /* themral expansivity: alpha0 = (1e-9,1e-4) */
        else if (i==4){
            if (org1 < 1e-9){ tmp = (org0-1e-9)/(org0-org1);  cout << "a0, " << tmp << " "; }
            if (org1 > 1e-4){ tmp = (1e-4-org0)/(org1-org0); }
        }
        /* pressure dependence of heat capaicty = (1e-3,1e-1) */
        else if (i==6){
            if (org1 < 1e-2){ tmp = (org0-1e-3)/(org0-org1);  cout << "C1, " << tmp << " "; }
            if (org1 > 1e+1){ tmp = (1e-1-org0)/(org1-org0); }
        }
        /* all pther parameters except Margules parameter at 0GPa */
        else if(i<7){
            if (v1[i] < 0){
                tmp = v0[i]/(v0[i]-v1[i]);
            }
        }
        
        /* if tmp is too small (= v[i] is trying to stick to the range limit we impose),
           just keep v[i] constant and remove it from the system for now.
           (tmp is set to 1 temporarily, b/c when mod < 1  
           no extension is allowed, which may cause the optimization to be ineffieicent)
	
           ... but this operation is only valid when determining the initial 
           extension direction (when bool isfirst = 1).
           when dv is changed during extension, it just messes up the search range.
           If isfirst == 0, simply use tmp as the adujsting constant (dv *= tmp) */
        if (isfirst){
            if (tmp <= 0){ dv[i] = 0.0;  tmp = 1; }
            else         { dv[i] *= tmp;          }
            cout << setw(9) << i << " [1st] " << tmp << endl;
        }else{
            mod = minv(tmp,mod);
        }
        
        /* just to be safe... */
        if (tmp < 0){ cout << "chi2CG.cpp/avoidneg: invalid tmp." << endl; exit(2); }
    }
    
    /* when avoidneg() is used in the latter part of opposite_end()
       ... shirnk dv by `mod` */
    if(!isfirst){
        dv = (dv*mod);
        cout << "  shrink by " << mod << endl;
    }
    
    return mod;
}
tensor1d<double> chi2SD_model::gss(double chi20, tensor1d<double> n0, 
                                   double chi21, tensor1d<double> n1){
    tensor1d<double> n_in = n0, nA, nB, n_out, ds;
    double chi2A, chi2B, tau = 0.61803398875;
    double chi2_in = chi20, chi2_ou = chi21;
    
    /* check two points: */
    ds = n1 - n0;
    nA = n0 + ds*(1-tau);   chi2A = calc_chi2(nA);
    nB = n0 + ds* tau;      chi2B = calc_chi2(nB);
    cout << "gss0:  " << minv(chi2A,chi2B) << "  " << endl;
    
    int refc = 0;  double res = 2, oldres = 1;
    while (refc < 15 && res > 1e-8){
        refc++;
        
        /* search between (nA,n1) */
        if (chi2A > chi2B){
            n0 = nA;  chi20 = chi2A;
            nA = nB;  chi2A = chi2B;
            ds = n1 -n0;
            nB = n0 +ds*tau;  chi2B = calc_chi2(nB);
            cout << "gss" << refc << " " << chi2B << "  ";
            show_par(nB, chi2B);
        }
        else{
            n1 = nB;  chi21 = chi2B;
            nB = nA;  chi2B = chi2A;
            ds = n1 -n0;
            nA = n0 +ds*(1-tau);  chi2A = calc_chi2(nA);
            cout << "gss" << refc << " " << chi2A << "  ";
            show_par(nA, chi2A);
        }
	
        /* termination condition*/
        oldres = res;    res = ds.norm();
    }
    cout << "  gss done, refc= " << refc << " ,res = " << res << endl;
    
    if (minv(chi2A,chi2B) < chi2_in){
        if (chi2A > chi2B){ n_out = nB;  chi2 = chi2B; }
        else              { n_out = nA;  chi2 = chi2A; }
    }else{
        n_out = n_in;  chi2 = chi2_in;
    }
    
    if (chi2 == chi2_in){
        tensor1d<double>  dmin(0.0,n_in.size());
        bool ismin = is_minimum(n_in, dmin);
        if (ismin){ cout << "chi2CG.cpp/gss failed." << endl; exit(3); }
        else { 
            tensor1d<double> dn = n1 - n0;
            cout << "perturb" << endl;
            n_out = n0 + dmin;
        }
    }
    
    return n_out;
}
bool chi2SD_model::is_minimum(const tensor1d<double>& n_par, tensor1d<double>& dmin){
    double chi2_org = calc_chi2(n_par);    chi2 = chi2_org;
    cout << "org: " << chi2_org << endl;
    
    tensor1d<double> eps(0.0,3);
    eps[0] = 1e-3;  eps[1] = 1e-4;  eps[2] = 1e-5;
    
    bool   ismin = 1;
    for (int j=0; j<(int)eps.size(); j++){
        for (int i=0; i<(int)n_par.size(); i++){
            tensor1d<double>  n_p = n_par, n_m = n_par;
            n_p[i] += eps[j];  n_m[i] -= eps[j];
	    
            double chi2_p = calc_chi2(n_p);
            double chi2_m = calc_chi2(n_m);
            cout << setw(3)  <<  i       << " - p: " << setw(12) << chi2_p-chi2_org;
            cout << " @ "    << setw(12) << n_p[i]*scale[i] << flush;
            cout << " , m: " << setw(12) << chi2_m-chi2_org;
            cout << " @ "    << setw(12) << n_m[i]*scale[i] << endl;
	    
            if (chi2_p-chi2_org < 0){     ismin = 0; dmin[i] = eps[j];        }
            else if(chi2_m-chi2_org < 0){ ismin = 0; dmin[i] = eps[j]*(-1.0); }
        }
        if (!ismin){ break; }
    }
    
    return ismin;
}
