/*
  SD_chi2.h
  ... inheritance of `melt_chi2` class

  created by Yoshi on Jul 5, 2018
*/

#ifndef MELT_CHI2SD_h
#define MELT_CHI2SD_h

#include "./melt_chi2.h"

class chi2SD_model : public melt_model {
 public:
    chi2SD_model();
    chi2SD_model& operator=(const chi2SD_model&);
    
    void             set_default_scale();
    void             set_rootCp_scale();
    void             show_par(tensor1d<double>&, double);
    tensor1d<double> optimize_SD();
    
 private:
    tensor1d<double>  par;
    tensor1d<double>  scale;
    
    double                      calc_chi2(){ return melt_model::calc_chi2(); };
    double                      calc_chi2(const tensor1d<double>&);
    tensor1d<double>                 grad(int, int, const tensor1d<double>&);
    tuple<tensor1d<double>, double>  neg_grad(const tensor1d<double>&);
    
    bool              is_minimum(const tensor1d<double>&, tensor1d<double>&);
    double            avoidneg(const tensor1d<double>&, tensor1d<double>&, bool);
    void              opposite_end(double&, tensor1d<double>&,
                                   double&, tensor1d<double>&, const tensor1d<double>&);
    tensor1d<double>  gss(double, tensor1d<double>, double, tensor1d<double>);
};

#endif
