# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
> This program calculates the phase-diagram for the (Mg,Fe)O-SiO2 pseudo-binary system.
> Solidus and liquidus temperatures with varying SiO2 content will be calculated.

* << input >>
> 1. pressure in GPa
> 2. Mg# = MgO/(MgO+FeO)

** THIS PROGRAM USES << mol% >>  NOT  << wt% >> !!! **

* << output >>
   The output file ("./diagram.txt") in each column represents
> 1. mole % of MgO
> 2. mole % of SiO2
> 3. mole % of FeO
> 4. solidus temperature
> 5. liquidus temperature

   The main file is `phase_diagram.cpp`.
   Please do not edit other files. If any erros occur please e-mail me (shown below).


### How do I get set up? ###

* Place everything in a single folder
* edit the compilor indicated in the first line of `MAKEFILE` (any g++ compiler)
* run `make`
* run ./condensation


### Who do I talk to? ###

* feel free to e-mail Yoshi Miyazaki (yoshinori.miyazaki@yale.edu)
