/*
  melt_chi2.cpp
  ... calculate the fit to the experimental data

  Jul 1, 2018
 */

#ifndef MELT_CHI2_h
#define MELT_CHI2_h

#include <tuple>
#include <chrono>
#include <random>
#include "omp.h"
#include "tensor.h"
#include "gibbse.h"
#include "CGgibbsmin.h"

using namespace std;

struct melting_TP{
    string            sys;
    double            P;
    double            T;
    double            Tobs;
    double            Terr;
    tensor1d<double>  comp;
};

class melt_model{
 public:
    melt_model();
    melt_model& operator=(const melt_model&);
    tensor1d<double> getW0(){return W0;};
    
    void    setW0(const tensor1d<double>& _W);
    void    install_data(string);
    void    perturb_data();
    
    double  calc_chi2();
    void    output(string);
    void    show_W0();
    
    double  mu_meltT(const string, const double, const tensor1d<double>&);
    
 protected:
    /* model parameters */
    double               chi2;
    tensor1d<double>     W0;

 private:
    /* data */
    tensor1d<melting_TP> exp_data;
    
    /* to calculate chi2 */
    double deltamu(const string, const tensor1d<double>&, const double, 
                   const double, tensor1d<double>);
    double eq_pvprc(const double, const tensor1d<double>&);
    double solve_ratio(const double, const double);
    tuple<tensor1d<double>,bool> Newton(const tensor1d<double>&, const tensor1d<double>&);
    tuple<tensor1d<double>,bool> Newton_exp(const tensor1d<double>&, const tensor1d<double>&);
};

#endif
