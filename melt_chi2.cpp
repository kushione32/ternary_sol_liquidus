/* 
  melt_chi2.cpp
  ... calculate the fit to the experimental data

  Jul 1, 2018
*/

#include "melt_chi2.h"

/*------------------------------------------------------------
  constructer & operator
  ------------------------------------------------------------*/
melt_model::melt_model(){
    W0.resize(0.0, 14);   chi2 = 1.0e10;
}
melt_model& melt_model::operator=(const melt_model& copy){
    if (this != &copy){
        exp_data = copy.exp_data;
        W0       = copy.W0;
        chi2     = copy.chi2;
    }
    return *this;
}

/*------------------------------------------------------------
  install data & parameter / perturb data
  ------------------------------------------------------------*/
void melt_model::install_data(string fname){
    /* read experimental data from file `fname` */
    ifstream fin(fname);
    if (!fin){ cout << "" << endl; exit(2); }
    
    tensor1d<melting_TP> data;
    melting_TP  dp;
    dp.comp.resize(3);
    while (!fin.eof()){
        fin >> dp.sys >> dp.P >> dp.T >> dp.Terr >> dp.comp[0] >> dp.comp[1] >> dp.comp[2];
        dp.Tobs = dp.T;
        data.push_back(dp);
    }
    
    /* store experimental data in the class object */
    exp_data = data;
}
void melt_model::perturb_data(){
    std::random_device  seed_gen;
    std::mt19937        mt_gen(seed_gen());
    std::normal_distribution<double>  norm_dist(0.0, 1.0);
    
    for (int i=0; i<(int)exp_data.size(); i++){
        exp_data[i].Tobs = exp_data[i].T + exp_data[i].Terr*(norm_dist(mt_gen));
        // cout << exp_data[i].T << " -> " << exp_data[i].Tobs << endl;
    }
}
void melt_model::setW0(const tensor1d<double>& W_init){
    if (W_init.size() == 14){
        W0 = W_init;   chi2 = 1e10;
    }else{
        cout << "melt_chi2.cpp ... melt_model(): wrong model size" << endl;
    }
}

/*------------------------------------------------------------
  calclate misfit
  ------------------------------------------------------------*/
double melt_model::calc_chi2(){
    double misfit = 0.0;
    
    //#pragma omp parallel for reduction(+:misfit)
    for (int i=0; i<(int)exp_data.size(); i++){
        string           spec = exp_data[i].sys;
        double           P    = exp_data[i].P * 1e9;
        double           Texp = exp_data[i].Tobs;
        double           Terr = exp_data[i].Terr;
        tensor1d<double> comp = exp_data[i].comp;
        
        double Tcal = mu_meltT(spec, P, comp);
        while (abs(Tcal-12000)<0.1){
            Tcal = mu_meltT(spec, P, comp);
        }
        // cout << spec << "\t" << P/1e9 << "\t" << Tcal << "\t" << Terr << "\t" << comp[0] << "\t" << comp[1] << "\t" << comp[2] ; //<< endl;
        // cout << " --- " << square((Tcal-Texp)/Terr) << endl;
        
        misfit += square((Tcal-Texp)/Terr);
    }
    
    chi2 = misfit;
    return chi2;
}
double melt_model::mu_meltT(const string spec, const double Pi, const tensor1d<double>& q){
    /* calculate Margules parameter */
    tensor1d<double> Wmix(0.0,2);
    Wmix[0] = W0[8]  + W0[9]*Pi;    Wmix[1] = W0[11] + W0[12]*Pi;
    
    /* in this function, melting T is determined using the bisection search. 
       ... however, there is some chance of muA = deltamu() being NAN. 
       ... in such a case, we rerun the bisection search for total of `MAX` times. */
    int count = 0, MAX = 10;
    double T0, T1, mu0, mu1, muA = 1, eps = 1e-2;
    while (std::isnan(muA) || (abs(muA) > eps)){
        count++;
        if (count == MAX){ break; }
        
        T0 = 300;   mu0 = deltamu(spec,q,T0,Pi,Wmix);
        T1 = 12000; mu1 = deltamu(spec,q,T1,Pi,Wmix);
        
        /* bisection search */
        while (abs(T1-T0) > 0.1){
            double TA = (T0+T1)/2.0;
            muA = deltamu(spec,q,TA,Pi,Wmix);
            // cout << TA << "\t" << Pi << "\t" << W0[8] << "\t" << Wmix[1] << endl;
            
            if (std::isnan(muA)){ T0 += 1.0; }
            else if(mu0*muA < 0){ T1 = TA;   }
            else                { T0 = TA;   }
        }
    }
    return (T0+T1)/2;
}
double melt_model::deltamu(const string spec, const tensor1d<double>& q, const double Ti, const double Pi, tensor1d<double> Wmix){
    /* mu(Mg-pv) + ln xMg = mu(MgO,l) + mu(SiO2,l)   ... (1)
       mu(Fe-pv) + ln xFe = mu(FeO,l) + mu(SiO2,l)   ... (2)
    calculate xMg + xFe from Eq.(1) & (2). */
    
    /* chemical potentials */
    tensor1d<double> G1(0.0,6);   G1[0] = W0[0];
    tensor1d<double> G2(0.0,6);   G2[0] = W0[1];
    tensor1d<double> G3(0.0,6);   for (int i=0; i<6; i++){ G3[i] = W0[i+2]; }
    
    Molecule_Gibbs m_MgOl("MgO(l)", Ti,Pi,G1);    double G_MgOl = m_MgOl.getGibbsE();
    Molecule_Gibbs m_FeOl("FeO(l)", Ti,Pi,G2);    double G_FeOl = m_FeOl.getGibbsE();
    Molecule_Gibbs m_SiO2("SiO2(l)",Ti,Pi,G3);    double G_SiO2 = m_SiO2.getGibbsE();
    
    /* mixing parameters */
    Wmix[0] -= W0[10]*(Ti-1500);   Wmix[1] -= W0[13]*(Ti-1500);
    Wmix /= (R*Ti);
    
    /* molar ratio of elements */
    double ntot = q.sum();
    double rMg = 0, rFe = 0, rSi = 0;
    rMg = q[0]/ntot;  rFe = q[2]/ntot;  rSi = q[1]/ntot;
    
    double actMg = ((1-rMg)*Wmix[0] - rFe*Wmix[1])*rSi;
    double actFe = ((1-rFe)*Wmix[1] - rMg*Wmix[0])*rSi;
    double actSi = (rMg*Wmix[0] + rFe*Wmix[1])*(rMg+rFe);
    
    double left, right;
    if(spec == "MgFeO_s"){
        Molecule_Gibbs m_Mgpc("MgO(p)",Ti,Pi,G1);     double G_Mgpc = m_Mgpc.getGibbsE();
        Molecule_Gibbs m_Fepc("FeO(p)",Ti,Pi,G1);     double G_Fepc = m_Fepc.getGibbsE();
        left  = G_Mgpc + log(rMg) - G_MgOl;
        right = G_Fepc + log(rFe) - G_FeOl;
        
    }else if(spec == "MgFeO_l"){
        Molecule_Gibbs m_Mgpc("MgO(p)",Ti,Pi,G1);     double G_Mgpc = m_Mgpc.getGibbsE();
        Molecule_Gibbs m_Fepc("FeO(p)",Ti,Pi,G1);     double G_Fepc = m_Fepc.getGibbsE();
        left  = G_MgOl + log(rMg) +actMg - G_Mgpc;
        right = G_FeOl + log(rFe) +actFe - G_Fepc;
        
    }else if(spec == "MgO_l"){	
        Molecule_Gibbs m_Mgpc("MgO(p)",Ti,Pi,G1);     double G_Mgpc = m_Mgpc.getGibbsE();
        left  = (G_MgOl +log(rMg) +actMg) - G_Mgpc;
        return left;
        
    }else if(spec == "FeO_l"){
        Molecule_Gibbs m_Fepc("FeO(p)",Ti,Pi,G1);     double G_Fepc = m_Fepc.getGibbsE();        
        left  = (G_FeOl +log(rFe) +actFe) - G_Fepc; 
        return left;
        
    }else if(spec == "SiO2_t"){
        Molecule_Gibbs m_stv("SiO2(t)",Ti,Pi,G1);     double G_stv  = m_stv.getGibbsE();
        left  = (G_SiO2 +log(rSi) +actSi) - G_stv;  // return left;
        return left;
        
    }else if(spec == "SiO2_f"){
        Molecule_Gibbs m_sef("SiO2(f)",Ti,Pi,G1);     double G_sef  = m_sef.getGibbsE();
        left  = (G_SiO2 +log(rSi) +actSi) - G_sef;
        return left;
        
    }else if(spec == "opx_l"){
        Molecule_Gibbs m_Mgpv("MgSiO3(p)",Ti,Pi,G1);  double G_Mgpv = m_Mgpv.getGibbsE();
        left  = G_MgOl + G_SiO2 + log(rMg) + log(rSi) + actMg + actSi;
        right = G_Mgpv;
        return left-right;
        
    }else if(spec == "opx_llP"){
        Molecule_Gibbs m_Mgop("MgSiO3(o)",Ti,Pi,G1);  double G_Mgop = m_Mgop.getGibbsE();
        left  = G_MgOl + G_SiO2 + log(rMg) + log(rSi) + actMg + actSi;
        right = G_Mgop;
        return left-right;
        
    }else if(spec == "fer_s"){
        Molecule_Gibbs m_Fepc("FeO(p)",Ti,Pi,G1);    double G_Fepc = m_Fepc.getGibbsE();
        Molecule_Gibbs m_stv("SiO2(t)",Ti,Pi,G1);    double G_stv  = m_stv.getGibbsE();
        
        double dSi = G_stv-G_SiO2,  dFe = G_Fepc-G_FeOl;
        
        left  = solve_ratio(dSi, Wmix[1]);
        right = solve_ratio(dFe, Wmix[1]);
        //cout << Ti << "K " << left << " , " << right << endl;
        
        return left+right-1;
        
    }else if(spec == "basalt_l"){
        /* just compare mu_stv & mu_SiO2 */
        Molecule_Gibbs m_stv("SiO2(t)",Ti,Pi,G1);     double G_stv  = m_stv.getGibbsE();
        left  = G_stv - (G_SiO2 + log(rSi) + actSi);
        return left;
        
    }else if(spec == "chondrite_l" || spec == "peridotite_l" || spec == "pyrolite_l"){
        Molecule_Gibbs m_Mgpv("MgSiO3(p)",Ti,Pi,G1);  double G_Mgpv = m_Mgpv.getGibbsE();
        Molecule_Gibbs m_Fepv("FeSiO3(p)",Ti,Pi,G1);  double G_Fepv = m_Fepv.getGibbsE();
        left  = G_MgOl + G_SiO2 - G_Mgpv + log(rMg) + log(rSi) + actMg + actSi;
        right = G_FeOl + G_SiO2 - G_Fepv + log(rFe) + log(rSi) + actFe + actSi;
        
    }else if(spec == "chondrite_llP1" || spec == "peridotite_llP1"){
        /* when MgSiO3(opx) is the remaining solid */
        Molecule_Gibbs m_Mgop("MgSiO3(o)",Ti,Pi,G1);  double G_Mgop = m_Mgop.getGibbsE();
        left  = G_Mgop; 
        right = G_MgOl + G_SiO2 + log(rMg) + log(rSi) + actMg + actSi;
        return left-right;
        
    }else if(spec == "chondrite_llP2" || spec == "peridotite_llP2"){
        Molecule_Gibbs m_Mgpc("MgO(p)",Ti,Pi,G1);     double G_Mgpc = m_Mgpc.getGibbsE();
        Molecule_Gibbs m_Fepc("FeO(p)",Ti,Pi,G1);     double G_Fepc = m_Fepc.getGibbsE();
        
        /* when (Mg,Fe)O is the remaining solid */
        left  = G_MgOl + log(rMg) + actMg - G_Mgpc;
        right = G_FeOl + log(rFe) + actFe - G_Fepc;
        
    }else if(spec == "chondrite_s" || spec == "peridotite_s" || spec == "pyrolite_s"){
        Molecule_Gibbs m_Mgpc("MgO(p)",Ti,Pi,G1);     double G_Mgpc = m_Mgpc.getGibbsE();
        Molecule_Gibbs m_Fepc("FeO(p)",Ti,Pi,G1);     double G_Fepc = m_Fepc.getGibbsE();
        Molecule_Gibbs m_Mgpv("MgSiO3(p)",Ti,Pi,G1);  double G_Mgpv = m_Mgpv.getGibbsE();
        Molecule_Gibbs m_Fepv("FeSiO3(p)",Ti,Pi,G1);  double G_Fepv = m_Fepv.getGibbsE();
        
        /* solve for the equilibrium assemblage of pv-prc without any melt
           ... by tracking dmu/dx the change in the GFE of the system.
           (mu: Gibbs energy, x: composition)
           
           Find equilibrium composition between
           << MgO + FeSiO3 <--> FeO + MgSiO3 >>
           using bisection search.
           dx denotes the amount of FeSiO3
           
           Two end-member cases:
           (n[FeO] < n[SiO2])
           MgO: q[0]-q[1],      FeO: q[2], MgSiO3: q[1],      FeSiO3: 0
           MgO: q[0]-q[1]+q[2]  FeO: 0,    MgSiO3: q[1]-q[2], FeSiO3: q[2]
           
           (n[FeO] < n[SiO2])
           MgO: q[0]-q[1], FeO: q[2],      MgSiO3: q[1],      FeSiO3: 0
           MgO: q[0]       FeO: q[2]-q[1], MgSiO3: 0,         FeSiO3: q[1]
        */
        
        double dmu = (G_Mgpc - G_Mgpv) - (G_Fepc - G_Fepv);
        
        int count = 0;
        double dx, dmudx, eps = 1.0e-10;  /* dx = the amount of Fe in perovskite! */
        double dx0 = maxv(q[1]-q[0],0.)+eps;
        double dmudx0 = dmu + eq_pvprc(dx0,q);
        double dx1 = minv(q[2]-eps, q[1]-eps), dmudx1 = dmu + eq_pvprc(dx1,q);
        // cout << "0: " << dmudx0 << " 1: " << dmudx1 << " dx = " << dx0 << endl;
        
        if (dmudx0 * dmudx1 > 0){ cout << "mcmc.cpp - eq_pvprc. bisec failed."; exit(2); }
        while (abs(dx1-dx0) > eps && count < 1000){
            count++;
            dx    = (dx0+dx1)/2;
            dmudx = dmu + eq_pvprc(dx,q);
            
            if (dmudx*dmudx0 < 0){ dx1 = dx;  dmudx1 = dmudx; }
            else                 { dx0 = dx;  dmudx0 = dmudx; }
        }
        
        tensor1d<double> delact(0.0,3);
        double npc = q[0]+q[2]-q[1], npv = q[1], nMgpc = q[0]+dx-q[1];
        delact[0] = G_Mgpc + log(nMgpc/npc)     - G_MgOl;  /* MgO(l) activity */
        delact[1] = G_Fepc + log((q[2]-dx)/npc) - G_FeOl;       /* FeO(l) activity */
        delact[2] = G_Mgpv + log((q[1]-dx)/npv) - (G_Mgpc + log(nMgpc/npc)) - G_SiO2;
        
        tensor1d<double> r;
        count = 0; bool accept = false;
        while (!accept){
            count++;
            if (count > 500)      { break; }
            else if (count%2 == 1){ tie(r, accept) = Newton(Wmix,delact); }
            else                  { tie(r, accept) = Newton_exp(Wmix,delact); }
        }
        
        // cout << r[0] << "  " << r[1] << "  " << r[2] << endl;
        return r.sum()-1;     /* left and right shd both be SiO2(l) + actSi at solidus */
        
    }else if(spec == "basalt_s"){
        Molecule_Gibbs m_Mgpc("MgO(p)",Ti,Pi,G1);     double G_Mgpc = m_Mgpc.getGibbsE();
        Molecule_Gibbs m_Fepc("FeO(p)",Ti,Pi,G1);     double G_Fepc = m_Fepc.getGibbsE();
        Molecule_Gibbs m_Mgpv("MgSiO3(p)",Ti,Pi,G1);  double G_Mgpv = m_Mgpv.getGibbsE();
        Molecule_Gibbs m_Fepv("FeSiO3(p)",Ti,Pi,G1);  double G_Fepv = m_Fepv.getGibbsE();
        
        /* solve for minimization of pv-prc */
        double dmu = (G_Mgpc - G_Mgpv) - (G_Fepc - G_Fepv);
        double dx, dmudx, eps = 1.0e-13;  /* dx = (0, nFe) */
        double dx0 = eps,      dmudx0 = dmu + eq_pvprc(dx0,q);
        double dx1 = q[2]-eps, dmudx1 = dmu + eq_pvprc(dx1,q);
        if (dmudx0 * dmudx1 > 0){ cout << "mcmc.cpp - eq_pvprc. bisec failed."; exit(2); }
        while (abs(dx1-dx0) > eps){
            dx    = (dx0+dx1)/2;
            dmudx = dmu + eq_pvprc(dx,q);
            
            if (dmudx*dmudx0 < 0){ dx1 = dx;  dmudx1 = dmudx; }
            else                 { dx0 = dx;  dmudx0 = dmudx; }
        }
        // cout << "@T = " << Ti << " Mgpc = " << q[0]-q[1]+dx << " - pv = " << q[1]-dx;
        
        /* now solve for melt composition */
        tensor1d<double> delact(0.0,3);
        double npc = q[0]+q[2]-q[1], npv = q[1], nMgpc = q[0]+dx-q[1];
        delact[0] = G_Mgpc + log(nMgpc/npc)     - G_MgOl;  /* MgO(l) activity */
        delact[1] = G_Fepc + log((q[2]-dx)/npc) - G_FeOl;       /* FeO(l) activity */
        delact[2] = G_Mgpv + log((q[1]-dx)/npv) - (G_Mgpc + log(nMgpc/npc)) - G_SiO2;
		
        tensor1d<double> r;
        int count = 0; bool accept = false;
        while (!accept){
            count++;
            if (count > 500)      { break; }
            else if (count%2 == 0){ tie(r, accept) = Newton(Wmix,delact); }
            else                  { tie(r, accept) = Newton_exp(Wmix,delact); }
        }
        
        return r.sum()-1;     /* left and right shd both be SiO2(l) + actSi at solidus */
    }
    
    return exp(left)+exp(right)-1;
}
double melt_model::eq_pvprc(const double dx, const tensor1d<double>& q){
    // double npc  = q[0] +q[2] -q[1], npv  = q[1];
    double nMgO = q[0] -q[1] +dx,   nFeO = q[2]-dx, nMgpv = q[1]-dx, nFepv = dx;
    double dxlogx = log(nMgO/nFeO) + log(nFepv/nMgpv);
    if (std::isnan(dxlogx)){
        cout << "dlog_dx = " << log(nMgO/nFeO) << " + " << log(nFepv/nMgpv) << " = " << dxlogx << endl;
        cout << "nMgO? " << nMgO << "\t" << nFepv << "\t" << nMgpv << endl;
    }
    return dxlogx;
}
double melt_model::solve_ratio(const double dmu, const double WFe){
    
    double r0 = 1.0e-100, r1 = 1, rA, delA;
    double del0 = log(r0) + (1-r0)*(1-r0)*WFe - dmu;
    double del1 = 0 - dmu;
    //cout << dmu << "  " << del0 << " " << del1 << endl;
    
    while (abs(r1-r0)>1.0e-3){
        rA   = (r0+r1)/2.0; 
        delA = log(rA) + (1-rA)*(1-rA)*WFe - dmu;
        
        if (del0*delA < 0){ r1 = rA;  del1 = delA; }
        else              { r0 = rA;  del0 = delA; }
    }
    //cout << dmu << " => " << rA << " is " << delA << endl;
    
    return rA;
}
tuple<tensor1d<double>, bool> melt_model::Newton(const tensor1d<double>& Wmix, const tensor1d<double>& A){
    /* solve for r(Mg), r(Fe), and r(Si) */
    tensor1d<double>  r(0.0,3), oldr(0.0,3);
    //cout << "del: " << A[0] << "  " << A[1] << "  " << A[2] << endl;
    //cout << "Wmi: " << Wmix[0] << "  " << Wmix[1] << endl;
	
    /* initial guess */
    for (int i=0; i<(int)r.size(); i++){ r[i] = (double)rand()/RAND_MAX; }
    
    int count = 0, MAX = 1e2;
    double absdr = 1, oldabs = 2, eps = 1.0e-9, lim = 1e-10;
    while (absdr > eps && count < MAX && oldabs != absdr){
        
        Matrix<double>   vA(0.0,3,3);
        Vector1d<double> vB(0.0,3);
        
        vA[0][0] = 1/r[0] - Wmix[0]*r[2];
        vA[0][1] =        - Wmix[1]*r[2];
        vA[0][2] =        - Wmix[1]*r[1] + Wmix[0]*(1-r[0]);
        vA[1][0] =        - Wmix[0]*r[2];
        vA[1][1] = 1/r[1] - Wmix[1]*r[2];
        vA[1][2] =        - Wmix[0]*r[0] + Wmix[1]*(1-r[1]);
        vA[2][0] = (1-r[2])*Wmix[0];
        vA[2][1] = (1-r[2])*Wmix[1];
        vA[2][2] = 1/r[2] - Wmix[0]*r[0] - Wmix[1]*r[1];
        
        vB[0] = log(r[0]) + ((1-r[0])*Wmix[0] - r[1]*Wmix[1])*r[2] - A[0];
        vB[1] = log(r[1]) + ((1-r[1])*Wmix[1] - r[0]*Wmix[0])*r[2] - A[1];
        vB[2] = log(r[2]) + (r[0]*Wmix[0] + r[1]*Wmix[1])*(1-r[2]) - A[2];
        vB *= (-1.0);
        
        Matrix<double>    luA = vA.lu_decomp();
        Matrix<double>    inA = luA.lu_inverse();
        tensor1d<double>  dr  = (inA*vB).to_tensor();
        
        oldr = r;        r += dr;
        oldabs = absdr;  absdr = dr.norm();
                
        for (int i=0; i<(int)r.size(); i++){ if (r[i] < 0){ r[i] = lim; }}
        count++;
    }
    
    /* convergence check */
    bool converge = false;
    if (std::isnan(absdr) || r.isnan()){ converge = false; }
    else if (count < MAX || oldabs == absdr || oldr == r){ converge = true; }
    
    return forward_as_tuple(r, converge);
}
tuple<tensor1d<double>, bool> melt_model::Newton_exp(const tensor1d<double>& Wmix, const tensor1d<double>& A){
    /* solve for r(Mg), r(Fe), and r(Si)
       ... In Newton_exp, x_Mg = log(r_Mg), r_Mg = exp(x_Mg) is solved.
           the result must be converted using exp at the end of the function. */
    
    /* set initial guess. */
    tensor1d<double>  r(0.0,3);
    for (int i=0; i<(int)r.size(); i++){ r[i] = log((double)rand()/RAND_MAX*1e-7); }
    
    int count = 0, MAX = 1e2;
    double absdr = 1, oldabs = 2, eps = 1.0e-10;
    while (absdr > eps && oldabs != absdr){        
        count++;
        if (count == MAX){ break; }

        Matrix<double>   vA(0.0,3,3);
        Vector1d<double> vB(0.0,3);
        
        vA[0][0] = 1 - Wmix[0]*exp(r[0]+r[2]);
        vA[0][1] =   - Wmix[1]*exp(r[1]+r[2]);
        vA[0][2] =  (- Wmix[1]*exp(r[1]) + Wmix[0]*(1-exp(r[0])))*exp(r[2]);
        vA[1][0] =   - Wmix[0]*exp(r[0]+r[2]);
        vA[1][1] = 1 - Wmix[1]*exp(r[1]+r[2]);
        vA[1][2] =  (- Wmix[0]*exp(r[0]) + Wmix[1]*(1-exp(r[1])))*exp(r[2]);
        vA[2][0] =     Wmix[0]*exp(r[0])*(1-exp(r[2]));
        vA[2][1] =     Wmix[1]*exp(r[1])*(1-exp(r[2]));
        vA[2][2] = 1 -(Wmix[0]*exp(r[0]+r[2]) + Wmix[1]*exp(r[1]+r[2]));
        
        vB[0] = r[0] + ((1-exp(r[0]))*Wmix[0] - exp(r[1])*Wmix[1])*exp(r[2]) - A[0];
        vB[1] = r[1] + ((1-exp(r[1]))*Wmix[1] - exp(r[0])*Wmix[0])*exp(r[2]) - A[1];
        vB[2] = r[2] + (exp(r[0])*Wmix[0] + exp(r[1])*Wmix[1])*(1-exp(r[2])) - A[2];
        vB *= (-1.0);
        
        Matrix<double>    luA = vA.lu_decomp();
        Matrix<double>    inA = luA.lu_inverse();
        tensor1d<double>  dr  = (inA*vB).to_tensor();
        
        r += dr;
        oldabs = absdr;  absdr = dr.norm();
    }
    
    /* check for convergence */
    bool converge = false;
    if (std::isnan(absdr) || r.isnan()){ converge = false; }
    else if (count < MAX || oldabs == absdr){ converge = true; }
    
    /* convert to x_Mg => r_Mg */
    for (int i=0; i<(int)r.size(); i++){ r[i] = exp(r[i]); }
    
    return forward_as_tuple(r, converge);
}
void melt_model::output(string fileout){
    ofstream fout(fileout,ios::app);
    for (int i=0; i<(int)W0.size(); i++){ fout << W0[i] << "  "; }
    fout << chi2 << endl;
    fout.close();
    
    // fileout.erase(fileout.begin(),fileout.begin()+4);
}
void melt_model::show_W0(){
    for (int i=0; i<(int)W0.size(); i++){ cout << W0[i] << "  "; }
    cout << " chi2= " << chi2 << endl;
}
